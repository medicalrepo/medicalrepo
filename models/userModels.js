const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        require:[true,"please tell us your name"],
    },
    email:{
        type:String,
        require:[true,"please provide our name!"],
        unique:true,
        lowercase:true,
        validate:[validator.isEmail, "please provide a valid email"],
    },
    photo:{
        type:String,
        default:'default.jpg',
    },
    role:{
        type:String,
        enum:['user','sme','pharmacist','admin'],
        default:'user',
    },
    password:{
        type:String,
        require:[true,'please provide password'],
        selecte:false,
    },
    passwordConfirm:{
        type:String,
        requuired:[true,"please confirm your password"],
        validate:{
            //this will work on save
            validator:function(el){
                return el ===this.password
            },
            message:"Password are not same"
        }
    },
    active:{
        type:Boolean,
        default:true,
        selecte:false
    },
})




//encryption of the password
userSchema.pre('save', async function(next){
    // only run this function if password was actually modified
    if (!this.isModified('password')) return next()

    // hash the password with cost of 12
    this.password = await bcrypt.hash(this.password,12)

    // Delete passwordConfirm Field
    this.passwordConfirm = undefined
    next()
})


// comparing the password provided my the user and the passwordin the database
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword,userPassword)
}



const User = mongoose.model('User',userSchema)
module.exports= User