const User = require('./../models/userModels')
const jwt = require('jsonwebtoken')
const promisify = require('util').promisify

const AppError = require('./../utils/appError')

const signToken = (id)=>{
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}


const createSendToken = (user,statusCode,res)=>{
    const token = signToken(user._id)
    const cookiesOption ={
        expires:new Date(
            Date.now()+process.env.JWT_COOKIES_EXPIRES_IN *24 *60*60*1000,
        ),
        httpOnly:true //this cookies cannot be access by the clientscripting
    }
    res.cookie('jwt',token,cookiesOption)//jwt - name of the cookies
    res.status(statusCode).json({
        status:'success',
        token,
        data:{
            user
        }
    })
}

exports.signup = async (req,res,next) => {
    try{
        const newUser = await User.create(req.body)
        createSendToken(newUser,201,res)    
    }catch(err){
        res.status(500).json({error:err.message})
    }  
} 



exports.login = async(req,res,next)=>{
    try{
        console.log(req.headers)
        const{email,password} = req.body
        //check if the password exist 
        if(!email || !password){
            return next(new AppError('please provide an email and passwod!',400))
        }

        //check if user exist && password is correct
        const user = await User.findOne({email}).select('+password')
        //const correct = await User.correctPassword(password,user.password)

        if(!user || !(await user.correctPassword(password,user.password))){//????????
            return next(new AppError('incorrect email or password',401))
        }
        createSendToken(user,200,res)
    }catch(err){
        res.status(500).json({error:err.message})
    }
}

exports.logout = (req,res) =>{
    res.cookie('token','',{
        expires:new Date(Date.now()+10*1000),
        httpOnly:true,
    })
    res.status(200).json({status:'success'})
}


exports.protect = async(req,res,next)=>{
    try{
        //1) getting token and check of its there
        let token 
        if(
            req.headers.authorization &&
            req.headers.authorization.startsWith('Bearer')
        ){
            token = req.headers.authorization.split(' ')[1]
            
        }else if(req.cookies.jwt){
            token = req.cookies.jwt

        }
        if(!token){
            return next(
                new AppError('You are not logged in! Please log in to get access.', 401),
            )
        }


        // 2) Verificatin token
        const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)
        console.log(decoded)


        // 3) check if user still exits
        const freshUser = await User.findById(decoded.id)
        if(!freshUser){
            return next(
                new AppError('the user belonging to this token no longer exist',401)
            )
        }
        //Grant access to protected route
        req.user =freshUser
        next()


    }catch(err){
        res.status(500).json({error:err.message})
    }
}

exports.updatePassword = async(req,res,next)=>{
    try{
        // 1) get the user from collection
        const user = await User.findById(req.user.id).select('+password')//?????????(req.user.id)

        // 2) check if posted current password is correct
        if(!(await user.correctPassword(req.body.passwordCurrent, user.password))){
            return next(new AppError('your current password is wrong'),401)
        }

        // 3) if so, update password
        user.password = req.body.password
        user.passwordConfirm = req.body.passwordConfirm
        await user.save()

        // 4) log user in, send jwt 
        createSendToken(user,200,res)
    }catch(err){
        res.status(500).json({error:err.message})
    }
}
